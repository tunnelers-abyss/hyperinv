
-- Check Inventory

minetest.register_privilege("inv", "Can use /inv")

minetest.register_chatcommand("inv", {
	params = "<name>",
	privs = {inv=true},
	description = "Shows inventory of <name>",
	func = function(name, param)
	      if param == nil or param == "" then
		  minetest.chat_send_player(name, param.. " is not connected or is not a known player.")
	      end
		
		local inv = minetest.get_inventory({ type="player", name=param })
		if not inv then
			minetest.chat_send_player(name, param.. " does not have inventory, maybe he is not connected or is not a known player")
		else
		      local detached_inv = minetest.create_detached_inventory(param,{
			    allow_move = function()
				return 0
			    end,
			    allow_put = function()
				return 0
			    end,
			    allow_take = function()
			    	return 0
		      	    end,
		      })
		        detached_inv:set_list('main', inv:get_list('main'))
        		minetest.show_formspec(name,'hyperinv','size[8,5]label[0,0;'..param..'`s inventory]'..'list[detached:'..param..';main;0,0.5;8,4;]button_exit[3,4.7;2,0.5;;Close]')
		 end


	end
})
